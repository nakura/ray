use crate::*;
use num_traits::float::Float;
#[cfg(test)]
use pretty_assertions::assert_eq;
use core::ops::{Add, Mul, Sub};

#[derive(Debug, PartialEq, Copy, Clone)]
pub struct Color<N: Float> {
    pub r: N,
    pub g: N,
    pub b: N,
}

impl<N: Float> Color<N> {
    fn new(r: N, g: N, b: N) -> Self {
        Self { r, g, b }
    }
}

oper_overload!(Add, add, +, Color<N>, N, r, g, b);
oper_overload!(Sub, sub, -, Color<N>, N, r, g, b);
oper_overload!(Mul, mul, *, Color<N>, N, r, g, b);

oper_overload_scalar!(Mul, mul, *, Color<N>, N, r, g, b);

#[cfg(feature = "std_unit_tests")]
#[cfg(test)]
#[test]
fn add_colors() {
    assert_eq!(
        Color::new(0.9, 0.6, 0.75) + Color::new(0.7, 0.1, 0.25),
        Color::new(1.6, 0.7, 1.0)
    )
}

#[test]
fn sub_colors() {
    assert_eq!(
        Color::new(0.2, 0.6, 0.75) - Color::new(0.1, 0.1, 0.25),
        Color::new(0.1, 0.5, 0.5)
    )
}

#[test]
fn mul_colors_by_scalar() {
    assert_eq!(Color::new(0.2, 0.3, 0.4) * 2.0, Color::new(0.4, 0.6, 0.8))
}

#[test]
fn mul_colors() {
    assert_eq!(
        Color::new(1.0, 0.2, 0.4) * Color::new(0.9, 1.0, 1.0),
        Color::new(0.9, 0.2, 0.4)
    )
}

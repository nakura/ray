use crate::*;
use num::{One, Zero};
use num_traits::float::Float;
#[cfg(test)]
use pretty_assertions::assert_eq;
use core::ops::{
    Add, AddAssign, Div, DivAssign, Index, IndexMut, Mul, MulAssign, Neg, Sub, SubAssign,
};

pub const EPSILON: f64 = 0.0001;

#[derive(Debug, PartialEq, Copy, Clone)]
pub struct Tuple<N: Float> {
    pub x: N,
    pub y: N,
    pub z: N,
    pub w: N,
}

impl<N> Tuple<N>
where
    N: Float + Zero + One,
{
    pub fn new(x: N, y: N, z: N, w: N) -> Self {
        Self { x, y, z, w }
    }

    pub fn vector(x: N, y: N, z: N) -> Self {
        Self {
            x,
            y,
            z,
            w: Zero::zero(),
        }
    }

    pub fn point(x: N, y: N, z: N) -> Self {
        Self {
            x,
            y,
            z,
            w: One::one(),
        }
    }

    pub fn sum(self) -> N {
        self.x + self.y + self.z + self.w
    }

    pub fn mag(self) -> N {
        (self * self).sum().sqrt()
    }

    pub fn norm(self) -> Self {
        self / self.mag()
    }

    pub fn dot(self, other: Self) -> N {
        (self * other).sum()
    }

    pub fn cross(self, other: Self) -> Self {
        Self {
            x: (self.y * other.z) - (self.z * other.y),
            y: (self.z * other.x) - (self.x * other.z),
            z: (self.x * other.y) - (self.y * other.x),
            w: self.w,
        }
    }
}

impl<N: Float> Index<usize> for Tuple<N> {
    type Output = N;

    fn index(&self, index: usize) -> &Self::Output {
        match index {
            0 => &self.x,
            1 => &self.y,
            2 => &self.z,
            3 => &self.w,
            i @ _ => unreachable!("Index for Tuple out of range: {}", i),
        }
    }
}

impl<N: Float> IndexMut<usize> for Tuple<N> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        match index {
            0 => &mut self.x,
            1 => &mut self.y,
            2 => &mut self.z,
            3 => &mut self.w,
            i @ _ => unreachable!("Index for Tuple out of range: {}", i),
        }
    }
}

impl<N: Float + Zero> Default for Tuple<N> {
    fn default() -> Self {
        Self {
            x: Zero::zero(),
            y: Zero::zero(),
            z: Zero::zero(),
            w: Zero::zero(),
        }
    }
}

impl<N: Float> Neg for Tuple<N> {
    type Output = Self;

    fn neg(self) -> Self::Output {
        Self {
            x: -self.x,
            y: -self.y,
            z: -self.z,
            w: -self.w,
        }
    }
}
#[derive(Debug)]
pub struct Environment<N: Float> {
    pub gravity: Tuple<N>,
    pub wind: Tuple<N>,
}

impl<N: Float> Environment<N> {
    pub fn new(gravity: Tuple<N>, wind: Tuple<N>) -> Self {
        Self { gravity, wind }
    }
}

#[derive(Debug)]
pub struct Projectile<N: Float> {
    pub position: Tuple<N>,
    pub velocity: Tuple<N>,
}

impl<N: Float> Projectile<N> {
    pub fn new(position: Tuple<N>, velocity: Tuple<N>) -> Self {
        Self { position, velocity }
    }
}

impl<N: Float + AddAssign> Projectile<N> {
    pub fn tick(&mut self, env: &Environment<N>) {
        self.position += self.velocity;
        self.velocity += env.gravity + env.wind;
    }
}

#[test]
#[ignore]
fn projectile() {
    let mut p = Projectile::new(
        Tuple::point(0.0, 1.0, 0.0),
        Tuple::vector(1.0, 1.0, 0.0).norm(),
    );

    let e = Environment::new(
        Tuple::vector(0.0, -0.1, 0.0),
        Tuple::vector(-0.01, 0.0, 0.0),
    );

    while p.position.y > 0.0 {
        p.tick(&e);
    }
}

oper_overload!(Add, add, +, Tuple<N>, N, x, y, z, w);
oper_overload!(Sub, sub, -, Tuple<N>, N, x, y, z, w);
oper_overload!(Mul, mul, *, Tuple<N>, N, x, y, z, w);
oper_overload!(Div, div, /, Tuple<N>, N, x, y, z, w);

oper_assign_overload!(AddAssign, add_assign, +=, Tuple<N>, N, x, y, z, w);
oper_assign_overload!(SubAssign, sub_assign, -=, Tuple<N>, N, x, y, z, w);
oper_assign_overload!(MulAssign, mul_assign, *=, Tuple<N>, N, x, y, z, w);
oper_assign_overload!(DivAssign, div_assign, /=, Tuple<N>, N, x, y, z, w);

oper_assign_overload_scalar!(MulAssign, mul_assign, *=, Tuple<N>, N, x, y, z, w);
oper_assign_overload_scalar!(DivAssign, div_assign, /=, Tuple<N>, N, x, y, z, w);

oper_overload_scalar!(Mul, mul, *, Tuple<N>, N, x, y, z, w);
oper_overload_scalar!(Div, div, /, Tuple<N>, N, x, y, z, w);

#[cfg(feature = "std_unit_tests")]
#[cfg(test)]
#[test]
fn tuple_new_sum() {
    assert_eq!(Tuple::new(2.0, 4.0, 6.0, 8.0).sum(), 20.0)
}

#[test]
fn mag_vec_1_0_0() {
    assert_eq!(Tuple::vector(1.0, 0.0, 0.0).mag(), 1.0)
}

#[test]
fn mag_vec_0_1_0() {
    assert_eq!(Tuple::vector(0.0, 1.0, 0.0).mag(), 1.0)
}

#[test]
fn mag_vec_0_0_1() {
    assert_eq!(Tuple::vector(0.0, 0.0, 1.0).mag(), 1.0)
}

#[test]
fn mag_vec_1_2_3() {
    assert_eq!(Tuple::vector(1.0, 2.0, 3.0).mag(), 14.0.sqrt())
}

#[test]
fn mag_neg_vec_1_2_3() {
    assert_eq!(Tuple::vector(-1.0, -2.0, -3.0).mag(), 14.0.sqrt())
}

#[test]
fn norm_vec_4_0_0() {
    assert_eq!(
        Tuple::vector(4.0, 0.0, 0.0).norm(),
        Tuple::vector(1.0, 0.0, 0.0)
    )
}

#[test]
fn norm_vec_1_2_3() {
    assert_eq!(
        Tuple::vector(1.0, 2.0, 3.0).norm(),
        Tuple::vector(1.0 / 14.0.sqrt(), 2.0 / 14.0.sqrt(), 3.0 / 14.0.sqrt())
    )
}

#[test]
fn tuple_new_dot_product() {
    assert_eq!(
        Tuple::vector(1.0, 2.0, 3.0).dot(Tuple::vector(2.0, 3.0, 4.0)),
        20.0
    )
}

#[test]
fn cross_product_vectors() {
    let a = Tuple::vector(1.0, 2.0, 3.0);
    let b = Tuple::vector(2.0, 3.0, 4.0);

    assert_eq!(a.cross(b), Tuple::vector(-1.0, 2.0, -1.0));

    assert_eq!(b.cross(a), Tuple::vector(1.0, -2.0, 1.0))
}

#[test]
fn neg_tuple() {
    assert_eq!(
        -Tuple::new(1.0, -2.0, 3.0, -4.0),
        Tuple::new(-1.0, 2.0, -3.0, 4.0)
    )
}

#[test]
fn mul_tuple_by_scalar() {
    assert_eq!(
        Tuple::new(1.0, -2.0, 3.0, -4.0) * 3.5,
        Tuple::new(3.5, -7.0, 10.5, -14.0)
    )
}

#[test]
fn div_tuple_by_scalar() {
    assert_eq!(
        Tuple::new(1.0, -2.0, 3.0, -4.0) / 2.0,
        Tuple::new(0.5, -1.0, 1.5, -2.0)
    );
}

#[cfg(test)]
#[test]
fn sub_two_points() {
    assert_eq!(
        Tuple::point(3.0, 2.0, 1.0) - Tuple::point(5.0, 6.0, 7.0),
        Tuple::vector(-2.0, -4.0, -6.0)
    )
}

#[test]
fn sub_vector_from_point() {
    assert_eq!(
        Tuple::point(3.0, 2.0, 1.0) - Tuple::vector(5.0, 6.0, 7.0),
        Tuple::point(-2.0, -4.0, -6.0)
    )
}

#[test]
fn sub_vector_from_vector() {
    assert_eq!(
        Tuple::point(3.0, 2.0, 1.0) - Tuple::vector(5.0, 6.0, 7.0),
        Tuple::point(-2.0, -4.0, -6.0)
    )
}

#[test]
fn sub_vector_from_zero_vector() {
    assert_eq!(
        Tuple::point(0.0, 0.0, 0.0) - Tuple::vector(1.0, -2.0, 3.0),
        Tuple::point(-1.0, 2.0, -3.0)
    )
}

#[test]
fn add_two_tuples() {
    assert_eq!(
        Tuple::new(3.0, -2.0, 5.0, 1.0) + Tuple::new(-2.0, 3.0, 1.0, 0.0),
        Tuple::new(1.0, 1.0, 6.0, 1.0)
    );
}

#[test]
fn check_vector_tuple() {
    assert_eq!(
        Tuple::new(4.0, -4.0, 3.0, 0.0),
        Tuple::vector(4.0, -4.0, 3.0)
    );
}

#[test]
fn check_point_tuple() {
    assert_eq!(
        Tuple::new(4.0, -4.0, 3.0, 1.0),
        Tuple::point(4.0, -4.0, 3.0)
    );
}

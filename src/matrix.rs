use crate::tuple::Tuple;
use num::{One, Zero};
use num_traits::float::Float;
#[cfg(test)]
use pretty_assertions::assert_eq;
use core::ops::{Index, IndexMut, Mul};

const D2X2: usize = 4;
const D3X3: usize = 9;
const D4X4: usize = 16;

#[derive(Debug, PartialEq, Copy, Clone)]
pub struct Matrix<N: Float, const S: usize> {
    cols: usize,
    rows: usize,
    data: [N; S],
}

impl<N: Float> Matrix<N, D2X2> {
    pub fn determinant(&self) -> N {
        (self[0][0] * self[1][1]) - (self[0][1] * self[1][0])
    }
}

impl<N: Float, const S: usize> Matrix<N, S>
where
    N: Zero + One,
{
    pub const fn new(cols: usize, rows: usize, data: [N; S]) -> Self {
        Self { cols, rows, data }
    }

    pub const fn default_square(width: usize, data: [N; S]) -> Self {
        Matrix::new(width, width, data)
    }

    pub const fn new_2x2(data: [N; D2X2]) -> Matrix<N, D2X2> {
        Matrix::new(2, 2, data)
    }

    pub const fn new_3x3(data: [N; D3X3]) -> Matrix<N, D3X3> {
        Matrix::new(3, 3, data)
    }

    pub const fn new_4x4(data: [N; D4X4]) -> Matrix<N, D4X4> {
        Matrix::new(4, 4, data)
    }

    pub fn id_matrix_4x4() -> Matrix<N, D4X4> {
        Matrix::<_, D4X4>::new_4x4([
            One::one(),
            Zero::zero(),
            Zero::zero(),
            Zero::zero(),
            Zero::zero(),
            One::one(),
            Zero::zero(),
            Zero::zero(),
            Zero::zero(),
            Zero::zero(),
            One::one(),
            Zero::zero(),
            Zero::zero(),
            Zero::zero(),
            Zero::zero(),
            One::one(),
        ])
    }

    pub fn transpose(&self) -> Self
    where
        [N; S]: Default,
    {
        let mut t = Matrix::new(self.cols, self.rows, Default::default());

        for r in 0..self.rows {
            for c in 0..self.cols {
                t[r][c] = self[c][r]
            }
        }

        t
    }
}

impl<N, const S: usize> Mul<Matrix<N, S>> for Matrix<N, S>
where
    [N; S]: Default,
    N: Float,
{
    type Output = Self;

    fn mul(self, rhs: Self) -> Self::Output {
        let mut m = Matrix::default_square(4, Default::default());

        for r in 0..self.rows {
            for c in 0..self.cols {
                m[r][c] = (0..self.rows)
                    .into_iter()
                    .fold(Zero::zero(), |acc, x| acc + (self[r][x] * rhs[x][c]))
            }
        }
        m
    }
}

impl<N, const S: usize> Mul<Tuple<N>> for Matrix<N, S>
where
    N: Float + Zero,
{
    type Output = Tuple<N>;

    fn mul(self, rhs: Tuple<N>) -> Self::Output {
        let mut t = Tuple::default();

        for r in 0..4 {
            t[r] = (0..4)
                .into_iter()
                .fold(Zero::zero(), |acc, n| acc + (self[r][n] * rhs[n]))
        }

        t
    }
}

impl<N: Float, const S: usize> Index<usize> for Matrix<N, S> {
    type Output = [N];

    fn index(&self, index: usize) -> &Self::Output {
        &self.data[index * self.cols..(index + 1) * self.cols]
    }
}

impl<N: Float, const S: usize> IndexMut<usize> for Matrix<N, S> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.data[index * self.cols..(index + 1) * self.cols]
    }
}

#[cfg(feature = "std_unit_tests")]
#[cfg(test)]
#[test]
fn construct_2_2_matrix() {
    let m = Matrix::<_, D2X2>::new_2x2([-3., 5., 1., -2.]);

    assert_eq!(m[0][0], -3.);
    assert_eq!(m[0][1], 5.);
    assert_eq!(m[1][0], 1.);
    assert_eq!(m[1][1], -2.);
}

#[test]
fn construct_3_3_matrix() {
    let m = Matrix::<_, D3X3>::new_4x4([
        1., 2., 3., 4., 5.5, 6.5, 7.5, 8.5, 9., 10., 11., 12., 13.5, 14.5, 15.5, 16.5,
    ]);

    assert_eq!(m[0][0], 1.);
    assert_eq!(m[0][3], 4.);
    assert_eq!(m[1][0], 5.5);
    assert_eq!(m[1][2], 7.5);
    assert_eq!(m[2][2], 11.);
    assert_eq!(m[3][0], 13.5);
    assert_eq!(m[3][2], 15.5);
}

#[test]
fn construct_4_4_matrix() {
    let m = Matrix::<_, D4X4>::new_4x4([
        1., 2., 3., 4., 5.5, 6.5, 7.5, 8.5, 9., 10., 11., 12., 13.5, 14.5, 15.5, 16.5,
    ]);

    assert_eq!(m[0][0], 1.);
    assert_eq!(m[0][3], 4.);
    assert_eq!(m[1][0], 5.5);
    assert_eq!(m[1][2], 7.5);
    assert_eq!(m[2][2], 11.);
    assert_eq!(m[3][0], 13.5);
    assert_eq!(m[3][2], 15.5);
}

#[test]
fn identical_matrix_equality() {
    let m1 = Matrix::<_, D4X4>::new_4x4([
        1., 2., 3., 4., 5., 6., 7., 8., 9., 8., 7., 6., 5., 5., 3., 2.,
    ]);

    let m2 = Matrix::<_, D4X4>::new_4x4([
        1., 2., 3., 4., 5., 6., 7., 8., 9., 8., 7., 6., 5., 5., 3., 2.,
    ]);

    assert_eq!(m1, m2);
}

#[test]
fn different_matrix_equality() {
    let m1 = Matrix::<_, D4X4>::new_4x4([
        1., 2., 3., 4., 5., 6., 7., 8., 9., 8., 7., 6., 5., 5., 3., 2.,
    ]);

    let m2 = Matrix::<_, D4X4>::new_4x4([
        2., 3., 4., 5., 6., 7., 8., 9., 8., 7., 6., 5., 5., 3., 2., 1.,
    ]);

    assert_ne!(m1, m2);
}

#[test]
fn mul_two_matrices() {
    let m1 = Matrix::<_, D4X4>::new_4x4([
        1., 2., 3., 4., 5., 6., 7., 8., 9., 8., 7., 6., 5., 4., 3., 2.,
    ]);

    let m2 = Matrix::<_, D4X4>::new_4x4([
        -2., 1., 2., 3., 3., 2., 1., -1., 4., 3., 6., 5., 1., 2., 7., 8.,
    ]);

    let m4 = Matrix::<_, D4X4>::new_4x4([
        20., 22., 50., 48., 44., 54., 114., 108., 40., 58., 110., 102., 16., 26., 46., 42.,
    ]);

    assert_eq!(m1 * m2, m4);
}

#[test]
fn mul_matrix_by_tuple() {
    let m = Matrix::<_, D4X4>::new_4x4([
        1., 2., 3., 4., 2., 4., 4., 2., 8., 6., 4., 1., 0., 0., 0., 1.0,
    ]);

    let t = Tuple::new(1., 2., 3., 1.);

    assert_eq!(m * t, Tuple::new(18.0, 24.0, 33.0, 1.0))
}

#[test]
fn mul_matrix_by_id_matrix() {
    let m = Matrix::<_, D4X4>::new_4x4([
        0., 1., 2., 4., 1., 2., 4., 8., 2., 3., 8., 16., 4., 8., 16., 32.,
    ]);

    assert_eq!(m * Matrix::<_, D4X4>::id_matrix_4x4(), m);
}

#[test]
fn mul_id_matrix_by_tuple() {
    let t = Tuple::new(1., 2., 3., 4.);

    assert_eq!(Matrix::<_, D4X4>::id_matrix_4x4() * t, t);
}

#[test]
fn transpose_id_matrix() {
    let id_m = Matrix::<f64, D4X4>::id_matrix_4x4();

    assert_eq!(id_m.transpose(), id_m);
}

#[test]
fn determinant_of_2x2_matrix() {
    let m = Matrix::<f64, D2X2>::new_2x2([1., 5., -3., 2.]);

    assert_eq!(m.determinant(), 17.);
}

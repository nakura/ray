#![feature(const_fn_trait_bound)]
#![no_std]
#[cfg(feature = "std_unit_tests")]
extern crate std;
pub mod tuple;
pub mod colors;
#[macro_use]
pub mod overload;
pub mod canvas;
pub mod matrix;
#[macro_use]
pub mod util;
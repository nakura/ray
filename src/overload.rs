#[macro_export]
macro_rules! oper_overload {
    ($oper_name:ident, $fname:ident, $oper:tt, $target:ty, $gen:tt, $($fields:ident),+) => {
      impl <$gen : Float> $oper_name<$target> for $target {
        type Output = Self;

        fn $fname(self, rhs: Self) -> Self::Output {
          Self {
            $($fields : self.$fields $oper rhs.$fields),+
          }
        }
      }
    };
  }

#[macro_export]
macro_rules! oper_assign_overload {
    ($oper_name:ident, $fname:ident, $oper:tt, $target:ty, $gen:tt, $($fields:ident),+) => {
      impl <$gen : Float + $oper_name> $oper_name<$target> for $target {
        fn $fname(&mut self, rhs: Self) {
            $(self.$fields $oper rhs.$fields);+
        }
      }
    };
  }

#[macro_export]
macro_rules! oper_overload_scalar {
      ($oper_name:ident, $fname:ident, $oper:tt, $target:ty, $gen:tt, $($fields:ident),+) => {
      impl <$gen : Float> $oper_name<$gen> for $target {
        type Output = Self;

        fn $fname(self, scalar: $gen) -> Self::Output {
          Self {
            $($fields : self.$fields $oper scalar),+
          }
        }
      }
    };
  }

#[macro_export]
macro_rules! oper_assign_overload_scalar {
    ($oper_name:ident, $fname:ident, $oper:tt, $target:ty, $gen:tt, $($fields:ident),+) => {
      impl <$gen : Float + $oper_name> $oper_name<$gen> for $target {
        fn $fname(&mut self, scalar: $gen) {
            $(self.$fields $oper scalar);+
        }
      }
    };
  }
